import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RateDto implements Serializable {
    private int code;
    private String msg;
    private Response[] response;
    private Info info;


}


